Source: avro-c
Priority: optional
Maintainer: Robert Edmonds <edmonds@debian.org>
Build-Depends:
 debhelper (>= 10),
 cmake,
 libjansson-dev (>= 2.3),
 liblzma-dev,
 libsnappy-dev,
 pkg-config,
 zlib1g-dev
Standards-Version: 4.3.0
Section: devel
Homepage: https://avro.apache.org
Vcs-Git: https://salsa.debian.org/edmonds/avro-c.git
Vcs-Browser: https://salsa.debian.org/edmonds/avro-c

Package: libavro-dev
Section: libdevel
Architecture: any
Depends: libavro23 (= ${binary:Version}), ${misc:Depends}
Description: Apache Avro C library headers (avro-c)
 Apache Avro is a data serialization system. Avro provides rich data
 structures; a binary data format; and a container file format, to store
 Avro-encoded data persistently.
 .
 This package provides the "avro-c" implementation of Apache Avro in C.
 The C implementation supports:
 .
  * binary encoding/decoding of all primitive and complex data types
  * storage to an Avro Object Container File
  * schema resolution, promotion and projection
  * validating and non-validating mode for writing Avro data
 .
 The C implementation of Avro lacks RPC support.
 .
 This package contains the development files.

Package: libavro23
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Apache Avro C shared library (avro-c)
 Apache Avro is a data serialization system. Avro provides rich data
 structures; a binary data format; and a container file format, to store
 Avro-encoded data persistently.
 .
 This package provides the "avro-c" implementation of Apache Avro in C.
 The C implementation supports:
 .
  * binary encoding/decoding of all primitive and complex data types
  * storage to an Avro Object Container File
  * schema resolution, promotion and projection
  * validating and non-validating mode for writing Avro data
 .
 The C implementation of Avro lacks RPC support.
 .
 This package contains the shared library.

Package: avro-bin
Section: utils
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Apache Avro C utilities (avro-c)
 Apache Avro is a data serialization system. Avro provides rich data
 structures; a binary data format; and a container file format, to store
 Avro-encoded data persistently.
 .
 This package provides the "avro-c" implementation of Apache Avro in C.
 The C implementation supports:
 .
  * binary encoding/decoding of all primitive and complex data types
  * storage to an Avro Object Container File
  * schema resolution, promotion and projection
  * validating and non-validating mode for writing Avro data
 .
 The C implementation of Avro lacks RPC support.
 .
 This package contains the avro-c command line utilities.
